const functions = require('firebase-functions');

const admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
});

exports.testeroni = functions.https.onCall(async (data, context) => {
  const { name } = data;

  return {
    text: name.toUpperCase(),
  };
});

exports.loginOrRegister = require('./loginOrRegister').default;
exports.fulfillPurchase = require('./fulfillPurchase').default;
exports.makePayLink = require('./makePayLink').default;
