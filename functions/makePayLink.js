const { URLSearchParams } = require('url');
const fetch = require('node-fetch');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const _ = require('lodash');
const addDays = require('date-fns/addDays');
const format = require('date-fns/format');

const db = admin.firestore();
const coursesRef = db.collection('courses');

/**
 * This function will create a pay link in Paddle for the particular course that
 * a user is trying to purchase. It will only do so if the user should actually
 * be able to purchase that course.
 */
exports.default = functions.https.onCall(async (data, context) => {
  // Ensure the user is actually has authentication information
  if (_.isNil(context) || _.isNil(context.auth)) {
    throw new functions.https.HttpsError('unauthenticated');
  }

  const uid = context.auth.uid;

  // By the time the user has called into this function, they've authenticated
  // with Firebase, so we should be able to find their account. We do this so
  // that we can get their email address since we'll require that later in this
  // function. Not every identity provider includes the email address in the
  // decoded token, but if they include it at all, then Firebase will have it.
  const userRecord = await admin.auth().getUser(uid);
  if (_.isNil(userRecord)) {
    throw new functions.https.HttpsError('not-found');
  }

  // We require that all users have an email address so that we can contact them
  // if needed.
  const { email } = userRecord;
  if (_.isNil(email) || _.isEmpty(email)) {
    throw new functions.https.HttpsError('failed-precondition');
  }

  const { courseId } = data;

  // Ensure that the course actually exists
  const courseDoc = await getCourseById(courseId);

  if (_.isNil(courseDoc)) {
    throw new functions.https.HttpsError('invalid-argument');
  }

  // TODO: validate that they don't already own the course

  const courseData = courseDoc.data();

  const payLinkUrl = await makePayLinkWithPaddle(uid, email, courseData);

  return { payLinkUrl };
});

/**
 * @param {string} courseId
 * @return {?DocumentSnapshot}
 */
async function getCourseById(courseId) {
  const querySnapshot = await coursesRef.where('id', '==', courseId).get();
  return _.head(querySnapshot.docs);
}

/**
 * Forms an expiration date one day in the future. The way that Paddle handles
 * this is to expire the link on midnight of that day.
 * @see https://developer.paddle.com/api-reference/product-api/pay-links/createpaylink
 */
function getLinkExpirationDate() {
  const expirationDate = addDays(new Date(), 1);

  return format(expirationDate, 'yyyy-MM-dd');
}

/**
 * Paddle has a format that's sort of hard to specify ahead of time, so we
 * instead specify it in an object like {USD: 19.99, EUR: 17.99}, then this
 * function will convert it into the format that Paddle expects.
 * @param {Object} pricesObject
 * @return {Array<Object>}
 * @see https://developer.paddle.com/api-reference/product-api/pay-links/createpaylink
 */
function convertPricesObjectToPaddleFormat(pricesObject) {
  const paddlePrices = [];

  let priceIndex = 0;
  _.forEach(pricesObject, (value, currencyCode) => {
    paddlePrices.push({
      key: `prices[${priceIndex}]`,
      value: `${currencyCode}:${value.toFixed(2)}`,
    });

    priceIndex++;
  });

  return paddlePrices;
}

/**
 * @param {Object} courseData - see courseSchema.js
 * @see https://developer.paddle.com/api-reference/product-api/pay-links/createpaylink
 */
async function makePayLinkWithPaddle(uid, email, courseData) {
  const vendorId = functions.config().paddle.vendor_id;
  const vendorAuthCode = functions.config().paddle.vendor_auth_code;

  const {
    id: courseId,
    paddleData: { title, custom_message, webhook_url, prices, image_url },
  } = courseData;

  const passthroughData = {
    uid,
    email,
    courseId,
  };

  const params = new URLSearchParams();
  params.append('vendor_id', vendorId);
  params.append('vendor_auth_code', vendorAuthCode);
  params.append('title', title);
  params.append('webhook_url', webhook_url);
  params.append('custom_message', custom_message);
  params.append('image_url', image_url);
  params.append('quantity_variable', '0');
  params.append('quantity', '1');
  params.append('expires', getLinkExpirationDate());
  params.append('marketing_consent', '0');
  params.append('customer_email', email);
  params.append('passthrough', JSON.stringify(passthroughData));
  params.append('discountable', '0');

  const pricesInPaddleFormat = convertPricesObjectToPaddleFormat(prices);
  _.forEach(pricesInPaddleFormat, ({ key, value }) => {
    params.append(key, value);
  });

  try {
    const res = await fetch(
      'https://vendors.paddle.com/api/2.0/product/generate_pay_link',
      {
        method: 'POST',
        body: params,
      }
    );
    const json = await res.json();
    if (!json.success) {
      console.error(
        'Got 200 but json.success is not true',
        JSON.stringify(json)
      );

      throw new functions.https.HttpsError('unknown');
    }

    return json.response.url;
  } catch (error) {
    console.error('Error calling generate_pay_link:', error);
    throw new functions.https.HttpsError('unknown');
  }
}
