const functions = require('firebase-functions');
const crypto = require('crypto');
const Serialize = require('php-serialize');
const admin = require('firebase-admin');
const _ = require('lodash');

const db = admin.firestore();
const purchasesRef = db.collection('purchases');
const usersRef = db.collection('users');
const coursesRef = db.collection('courses');

exports.default = functions.https.onRequest(async (req, res) => {
  const jsonObject = req.body;
  const isValidSignature = validateWebhook(jsonObject);

  // Paddle itself shouldn't ever have an invalid signature, but anyone trying
  // to maliciously poke at this endpoint WOULD have an invalid signature.
  if (!isValidSignature) {
    return res.status(400).send();
  }

  await fulfillPurchase(jsonObject);

  return res.status(200).send();
});

/**
 * This is called by Paddle's fulfillment webhook. There are two very important
 * facts to know here:
 *
 * 1. Paddle will just keep calling into the webhook until it's successful or
 *    until 3 days have passed.
 * 2. The customer was already charged at this point.
 *
 * This means that we can't exactly throw errors here like we would if we were
 * validating this BEFORE the funds were debited; the user was already charged,
 * so we need to track this fact, and ideally, we'll actually fulfill the
 * purchase if we can.
 *
 * There's no guarantee that we can fulfill the purchase for a number of
 * reasons, e.g. the user may have deleted their account already, or maybe they
 * already own what they're buying. Those are cases for refunds, not for
 * throwing errors from this function.
 *
 * The only time you SHOULD throw an error from this function is if you
 * absolutely cannot process the data, e.g. it's in an unexpected format and
 * there's nothing we can salvage.
 * @see https://developer.paddle.com/webhook-reference/intro
 * @param {Object} jsonObject - an already-parsed JSON object from Paddle. See
 * https://developer.paddle.com/webhook-reference/product-fulfillment/fulfillment-webhook
 * for more information.
 */
async function fulfillPurchase(jsonObject) {
  // The passthrough data was made by us in makePayLink, so we can parse it.
  const parsedPassthroughData = JSON.parse(jsonObject.passthrough);

  // Overwrite the original passthrough with its parsed version so that
  // Firestore can index it properly, e.g. to allow searching for purchases from
  // a particular user.
  delete jsonObject.passthrough;
  jsonObject.parsedPassthroughData = parsedPassthroughData;

  await storePurchaseRecord(jsonObject);
  await grantCourseToUser(jsonObject);
}

/**
 * Adds or updates information about a purchase itself (even if we can't fulfill
 * it later on for whatever reason).
 *
 * Note that the only case I can think of where we would be UPDATING this
 * instead of ADDING it is if there's a JavaScript failure or timeout after this
 * function call, so Paddle would need to keep hitting the webhook with the same
 * data. Even in that case, I don't think the data would change in between
 * calls, so the update is probably unnecessary.
 * @param {Object} jsonObject
 */
async function storePurchaseRecord(jsonObject) {
  const { p_order_id } = jsonObject;

  await db.runTransaction(async (trx) => {
    const purchaseQuerySnapshot = await trx.get(
      purchasesRef.where('p_order_id', '==', p_order_id)
    );
    const purchaseDoc = _.head(purchaseQuerySnapshot.docs);

    // If this is being called for the first time with this order, then create
    // it, otherwise update it. Note: the only case I can think of where Paddle
    // would update this is if there's a JavaScript failure or timeout after
    // this happens and it needs to keep hitting the webhook with the same data.
    // Even in that case, I don't think the data would change in between calls,
    // so the update is probably unnecessary.
    if (_.isNil(purchaseDoc)) {
      trx.set(purchaseRef.doc(), jsonObject);
    } else {
      trx.update(purchaseDoc.ref, jsonObject);
    }
  });
}

/**
 * Attempts to grant a course to a user.
 * @param {Object} jsonObject
 */
async function grantCourseToUser(jsonObject) {
  const {
    p_order_id,
    parsedPassthroughData: { uid, courseId },
  } = jsonObject;

  await db.runTransaction(async (trx) => {
    const userQuerySnapshot = await trx.get(usersRef.where('uid', '==', uid));
    const userDoc = _.head(userQuerySnapshot.docs);

    const courseQuerySnapshot = await trx.get(
      coursesRef.where('id', '==', courseId)
    );
    const courseDoc = _.head(courseQuerySnapshot.docs);

    if (_.isNil(userDoc)) {
      console.error(
        `[REFUND ALERT] refund candidate detected for p_order_id=${p_order_id} because the user doesn't exist any longer. Bailing out.`
      );
      return;
    }

    // Note: we explicitly do NOT return from this particular condition so that
    // we end up saving this course to the user even though the course no longer
    // exists. We know it had to exist in the first place because the server
    // sent a valid ID to the user via Paddle, and we made sure Paddle's
    // signature held up, so the client couldn't have tampered with this.
    if (_.isNil(courseDoc)) {
      console.error(
        `[REFUND ALERT] refund candidate detected for p_order_id=${p_order_id} because the course doesn't exist any longer. Not bailing out since we'll still end up granting the user this course.`
      );
    }

    // The user still exists, so let's grant them the course.
    const { purchasedCourses } = userDoc.data();
    if (_.includes(purchasedCourses, courseId)) {
      console.error(
        `[REFUND ALERT] refund candidate detected for p_order_id=${p_order_id} because the user already owns that course. Bailing out.`
      );
      return;
    }

    purchasedCourses.push(courseId);
    console.log(`Successfully granted course ID=${courseId} to uid=${uid}`);

    trx.update(userDoc.ref, { purchasedCourses });
  });
}

// Public keys are base64-encoded, so backslashes won't naturally appear, so any
// literal "\\n" sequences indicate that I added a newline just so that Firebase
// would accept it in the config.
const pubKey = functions
  .config()
  .paddle.seller_public_key.replace(/\\n/g, '\n');

function ksort(obj) {
  const keys = Object.keys(obj).sort();
  let sortedObj = {};

  for (let i in keys) {
    sortedObj[keys[i]] = obj[keys[i]];
  }

  return sortedObj;
}

function validateWebhook(jsonObj) {
  const mySig = Buffer.from(jsonObj.p_signature, 'base64');
  delete jsonObj.p_signature;
  // Need to serialize array and assign to data object
  jsonObj = ksort(jsonObj);
  for (let property in jsonObj) {
    if (
      // I'm disabling eslint here because this was copy/pasted code, but
      // Object.prototype.hasOwnProperty.call(foo, "bar"); should be fine.
      //
      // eslint-disable-next-line no-prototype-builtins
      jsonObj.hasOwnProperty(property) &&
      typeof jsonObj[property] !== 'string'
    ) {
      if (Array.isArray(jsonObj[property])) {
        // is it an array
        jsonObj[property] = jsonObj[property].toString();
      } else {
        //if its not an array and not a string, then it is a JSON obj
        jsonObj[property] = JSON.stringify(jsonObj[property]);
      }
    }
  }
  const serialized = Serialize.serialize(jsonObj);
  // End serialize data object
  const verifier = crypto.createVerify('sha1');
  verifier.update(serialized);
  verifier.end();

  const verification = verifier.verify(pubKey, mySig);

  return verification;
}
