import PropTypes from 'prop-types';
import React from 'react';
import Button from '../components/Button';
import classNames from 'classnames';
import { OpenIdProvider } from '../misc/constants';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const openIdIconInfo = {
  [OpenIdProvider.GOOGLE]: {
    iconColor: 'text-googleRed',
    icon: ['fab', 'google'],
    text: 'Google',
  },
  [OpenIdProvider.TWITCH]: {
    iconColor: 'text-twitchPurple',
    icon: ['fab', 'twitch'],
    text: 'Twitch',
  },
  [OpenIdProvider.PATREON]: {
    iconColor: 'text-patreonPeach',
    icon: ['fab', 'patreon'],
    text: 'Patreon',
  },
  [OpenIdProvider.DISCORD]: {
    iconColor: 'text-discordBlue',
    icon: ['fab', 'discord'],
    text: 'Discord',
  },
  [OpenIdProvider.TWITTER]: {
    iconColor: 'text-twitterBlue',
    icon: ['fab', 'twitter'],
    text: 'Twitter',
  },
  [OpenIdProvider.GITLAB]: {
    iconColor: 'text-gitlabOrange',
    icon: ['fab', 'gitlab'],
    text: 'GitLab',
  },
};

/**
 * An OpenID button for something like "Log in with Google".
 */
export default function OpenIDButton({ openIdProvider, onClick }) {
  const { iconColor, icon, text } = openIdIconInfo[openIdProvider];
  return (
    <Button
      onClick={() => onClick(openIdProvider)}
      className="m-2 border w-22 h-22 flex flex-col p-4 items-center rounded-md border-gray-300"
    >
      <div>
        <FontAwesomeIcon
          className={classNames(iconColor, 'w-6 mb-2')}
          icon={icon}
        />
      </div>
      <div className="font-bold ">{text}</div>
    </Button>
  );
}

OpenIDButton.propTypes = {
  openIdProvider: PropTypes.oneOf(_.values(OpenIdProvider)).isRequired,
  onClick: PropTypes.func.isRequired,
};
