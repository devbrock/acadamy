import React from 'react';

import { useAuth } from '../hooks/auth.js';

export default function Course(props) {
  const auth = useAuth();

  return (
    <div>
      {auth.acAdamyUser ? (
        <>
          <div>You are logged in</div>
          <button
            className="border-solid border-black border-2"
            onClick={() => auth.testeroni('adam')}
          >
            Testeroni
          </button>
          <button
            className="border-solid border-black border-2"
            onClick={auth.logout}
          >
            Logout
          </button>
        </>
      ) : (
        <>
          <div>
            You are <strong>not</strong> logged in
            <button
              className="border-solid border-black border-2"
              onClick={auth.registerWithTwitter}
            >
              Register with Twitter
            </button>
          </div>
        </>
      )}
    </div>
  );
}
