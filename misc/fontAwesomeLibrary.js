import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faGoogle,
  faTwitch,
  faPatreon,
  faDiscord,
  faTwitter,
  faGitlab,
  faPaypal,
  faGoogleWallet,
  faYoutube,
  faInstagram,
  faFacebook,
} from '@fortawesome/free-brands-svg-icons';
import {
  faCheckSquare,
  faCoffee,
  faBook,
  faShoppingCart,
  faCreditCard,
  faArrowRight,
  faUserCircle,
  faUnlink,
  faCheckCircle,
  faTrash,
  faWallet,
  faStar,
  faExternalLinkAlt,
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faGoogle,
  faTwitch,
  faPatreon,
  faDiscord,
  faTwitter,
  faGitlab,
  faCheckSquare,
  faCoffee,
  faBook,
  faShoppingCart,
  faCreditCard,
  faArrowRight,
  faPaypal,
  faGoogleWallet,
  faUserCircle,
  faUnlink,
  faCheckCircle,
  faTrash,
  faWallet,
  faStar,
  faExternalLinkAlt,
  faYoutube,
  faInstagram,
  faFacebook
);
