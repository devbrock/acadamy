import { createEnum } from './util';
import assert from 'assert';
import _ from 'lodash';

/**
 * @enum {string}
 */
export const OpenIdProvider = createEnum([
  'GOOGLE',
  'TWITCH',
  'PATREON',
  'DISCORD',
  'TWITTER',
  'GITLAB',
]);

/**
 * These are a subset of OpenIdProvider that are allowed for logging or
 * registration.
 */
export const OpenIdProvidersForLoginOrRegistration = [
  OpenIdProvider.GOOGLE,
  OpenIdProvider.TWITTER,
];

// Make sure there are no null values
assert(
  _.isEmpty(
    _.difference(
      OpenIdProvidersForLoginOrRegistration,
      _.values(OpenIdProvider)
    )
  ),
  "An identity provider was specified for login/registration that doesn't exist."
);

/**
 * @enum {string}
 */
export const CourseId = createEnum(['PLACEHOLDER']);
