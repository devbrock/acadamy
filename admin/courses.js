const admin = require('firebase-admin');
const _ = require('lodash');
const path = require('path');
const jsonc = require('jsonc');
const { courseSchema } = require('./courseSchema');

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
});

const db = admin.firestore();
const coursesRef = db.collection('courses');

async function listCourses() {
  const querySnapshot = await coursesRef.listDocuments();
  querySnapshot.forEach((documentReference) => {
    console.log(
      'documentReference.path: ' + JSON.stringify(documentReference.path)
    );
  });
}

async function getAllCourses() {
  const querySnapshot = await coursesRef.get();
  querySnapshot.forEach((documentSnapshot) => {
    const courseData = documentSnapshot.data();
    const { id } = courseData;
    console.log(`${id}:`);
    console.log(JSON.stringify(courseData, null, 2));
    console.log();
  });
}

async function deleteCourse(courseId) {
  if (_.isNil(courseId)) {
    console.error(`Usage: node ${getThisFileNameNoExt()} del COURSE_ID`);
    return;
  }

  const courseDoc = await getCourseById(courseId);
  if (_.isNil(courseDoc)) {
    console.log(`No course found with ID=${courseId}`);
    return;
  }

  // Print it first in case they didn't mean to delete it.
  console.log(`Found course with ID=${courseId}:`);
  const courseData = courseDoc.data();
  console.log(JSON.stringify(courseData, null, 2));

  await courseDoc.ref.delete();
  console.log('Deleted it');
}

async function getSingleCourse(courseId) {
  if (_.isNil(courseId)) {
    console.error(`Usage: node ${getThisFileNameNoExt()} get COURSE_ID`);
    console.error(`(or just use "getall" with no argument)`);
    return;
  }

  const courseDoc = await getCourseById(courseId);
  if (_.isNil(courseDoc)) {
    console.log(`No course found with ID=${courseId}`);
    return;
  }

  console.log(`Found course with ID=${courseId}:`);
  const courseData = courseDoc.data();
  console.log(JSON.stringify(courseData, null, 2));
}

function getThisFileNameNoExt() {
  return path.basename(__filename, path.extname(__filename));
}

function printOverallUsage() {
  console.error(
    `Usage: node ${getThisFileNameNoExt()} list|get|getall|del|update|add`
  );
}

async function getCourseById(courseId) {
  const querySnapshot = await coursesRef.where('id', '==', courseId).get();
  return _.head(querySnapshot.docs);
}

async function updateCourse(pathToJsonFile) {
  if (_.isNil(pathToJsonFile)) {
    console.error(
      `Usage: node ${getThisFileNameNoExt()} update PATH_TO_JSON_FILE`
    );
    console.error(
      'Look at course_template.jsonc for the structure of that file.'
    );
    return;
  }

  const jsonObj = await jsonc.read(pathToJsonFile);
  const validatedCourse = await courseSchema.validate(jsonObj);
  const courseId = validatedCourse.id;

  // Ensure the course already exists
  const existingCourseDoc = await getCourseById(courseId);
  if (_.isNil(existingCourseDoc)) {
    console.error(
      `Course ${courseId} doesn't exist, so we can't update it. Call "add" first.`
    );
    return;
  }

  await existingCourseDoc.ref.set(validatedCourse);

  console.log(`Updated course ID=${courseId} in Firestore:`);
  console.log(JSON.stringify(validatedCourse, null, 2));
}

async function addCourse(pathToJsonFile) {
  if (_.isNil(pathToJsonFile)) {
    console.error(
      `Usage: node ${getThisFileNameNoExt()} add PATH_TO_JSON_FILE`
    );
    console.error(
      'Look at course_template.jsonc for the structure of that file.'
    );
    return;
  }

  const jsonObj = await jsonc.read(pathToJsonFile);
  const validatedCourse = await courseSchema.validate(jsonObj);
  const courseId = validatedCourse.id;

  // Ensure the course doesn't already exist
  const existingCourseDoc = await getCourseById(courseId);
  if (!_.isNil(existingCourseDoc)) {
    console.error(
      `Course ${courseId} already exists. Not adding again (call "update" instead).`
    );
    return;
  }

  await coursesRef.add(validatedCourse);

  console.log(`Added course ID=${courseId} to Firestore:`);
  console.log(JSON.stringify(validatedCourse, null, 2));
}

async function main() {
  const args = process.argv.slice(2);
  if (_.isEmpty(args)) {
    printOverallUsage();
    return;
  }

  const command = args[0].toLowerCase();
  if (command === 'list') {
    await listCourses();
  } else if (command === 'add') {
    await addCourse(args[1]);
  } else if (command === 'update') {
    await updateCourse(args[1]);
  } else if (command === 'getall') {
    await getAllCourses();
  } else if (command === 'get') {
    await getSingleCourse(args[1]);
  } else if (command === 'del') {
    await deleteCourse(args[1]);
  } else {
    console.error(`Unrecognized command: ${command}`);
    printOverallUsage();
  }
}

main().then(() => {
  process.exit(0);
});
