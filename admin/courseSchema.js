const yup = require('yup');
const _ = require('lodash');

// https://paddle.com/support/what-currencies-do-you-support/
const validCurrencies = [
  'ARS',
  'AUD',
  'BRL',
  'GBP',
  'CAD',
  'CNY',
  'CZK',
  'DKK',
  'EUR',
  'HKD',
  'HUF',
  'INR',
  'JPY',
  'MXN',
  'TWD',
  'NZD',
  'PLN',
  'RUB',
  'SGD',
  'ZAR',
  'KRW',
  'SEK',
  'CHF',
  'USD',
];

const currencySchema = yup.number().positive();

const pricesShape = _.reduce(
  validCurrencies,
  (accum, currency) => {
    accum[currency] = currencySchema;
    return accum;
  },
  {}
);

const courseSchema = yup
  .object()
  .noUnknown()
  .strict()
  .shape({
    id: yup.string().min(1).required(),

    // https://developer.paddle.com/api-reference/product-api/pay-links/createpaylink
    paddleData: yup
      .object()
      .required()
      .noUnknown()
      .strict()
      .shape({
        title: yup.string().max(200).required(),
        custom_message: yup.string().max(255).required(),
        webhook_url: yup.string().url().required(),
        prices: yup.object().strict().noUnknown().shape(pricesShape).required(),
        image_url: yup.string().url(),
      }),
  });

module.exports = {
  courseSchema,
};
