import React, { useState, useEffect, useContext, createContext } from 'react';
import {
  OpenIdProvidersForLoginOrRegistration,
  OpenIdProvider,
} from '../misc/constants';
import _ from 'lodash';
import { useRouter } from 'next/router';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';

const firebaseConfig = {
  apiKey: process.env.NEXT_STATIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_STATIC_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.NEXT_STATIC_FIREBASE_DATABASE_URL,
  projectId: process.env.NEXT_STATIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_STATIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.NEXT_STATIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.NEXT_STATIC_FIREBASE_APP_ID,
  measurementId: process.env.NEXT_STATIC_FIREBASE_MEASUREMENT_ID,
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
  firebase.functions().useFunctionsEmulator('http://localhost:5001');
}

const AuthContext = createContext();

// eslint-disable-next-line react/prop-types
export function AuthProvider({ children }) {
  const auth = useAuthProvider();

  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
  return useContext(AuthContext);
};

function useAuthProvider() {
  // The user has an identity with Firebase, where it's just authentication
  // information, and it also has extra data from the database.
  const [acAdamyUser, setAcAdamyUser] = useState(null);
  const [firebaseUser, setFirebaseUser] = useState(null);
  const router = useRouter();

  async function loginOrRegisterWithProvider(openIdProvider) {
    if (!_.includes(OpenIdProvidersForLoginOrRegistration, openIdProvider)) {
      throw new Error(
        `Cannot log in with ${openIdProvider} since they are not one of ${OpenIdProvidersForLoginOrRegistration}`
      );
    }

    let provider = null;

    switch (openIdProvider) {
      case OpenIdProvider.TWITTER:
        provider = new firebase.auth.TwitterAuthProvider();
        break;
      case OpenIdProvider.GOOGLE:
        provider = new firebase.auth.GoogleAuthProvider();
        break;
    }

    // To apply the default browser preference instead of explicitly setting it.
    firebase.auth().useDeviceLanguage();

    try {
      const result = await firebase.auth().signInWithPopup(provider);
      // This gives you a Google Access Token. You can use it to access the Google API.
      // var token = result.credential.accessToken;
      // The signed-in user info.
      var firebaseUser = result.firebaseUser;
      setFirebaseUser(firebaseUser);
      // ...
      console.log('Success');

      // var twitterProvider = new firebase.auth.TwitterAuthProvider();
      // const twitterResult = await firebase
      //   .auth()
      //   .currentUser.linkWithPopup(twitterProvider);

      // console.log('Calling into loginOrRegister');
      // await loginOrRegisterUserWithDatabase();
      // const timestamp = new firebase.firestore.Timestamp(
      //   testResult.data.createdAt._seconds,
      //   testResult.data.createdAt._nanoseconds
      // );
    } catch (error) {
      // Handle Errors here.
      // var errorCode = error.code;
      // var errorMessage = error.message;
      // // The email of the user's account used.
      // var email = error.email;
      // // The firebase.auth.AuthCredential type that was used.
      // var credential = error.credential;
      // ...

      console.error('Error', error);
    }

    // For now, just unconditionally put them back on the landing page.
    router.replace('/');

    return firebaseUser;
  }

  async function testeroni(param) {
    const testeroniFn = firebase.functions().httpsCallable('testeroni');
    const result = await testeroniFn({ name: param });
    return result.data.text;
  }

  async function logout() {
    await firebase.auth().signOut();

    setFirebaseUser(null);
  }

  async function loginOrRegisterUserWithDatabase() {
    const loginOrRegisterFn = firebase
      .functions()
      .httpsCallable('loginOrRegister');
    const loggedInUser = await loginOrRegisterFn();
    setAcAdamyUser(loggedInUser);
  }

  // Track any user auth stuff changes
  useEffect(() => {
    const unsubscribe = firebase
      .auth()
      .onAuthStateChanged(async (firebaseUser) => {
        if (firebaseUser) {
          setFirebaseUser(firebaseUser);

          // // At any point, we may have just authenticated with Firebase, and
          // // when we do that, we want to fetch site-specific information about
          // // the user..
          // if (_.isNil(acAdamyUser)) {
          //   await loginOrRegisterUserWithDatabase();
          // }
        } else {
          setFirebaseUser(null);
          setAcAdamyUser(null);
        }
      });

    return () => unsubscribe();
  }, []);

  // Track any user auth stuff changes
  useEffect(() => {
    async function updateAcAdamyUser() {
      // At any point, we may have just authenticated with Firebase, and when we
      // do that, we want to fetch site-specific information about the user..
      if (_.isNil(acAdamyUser) && !_.isNil(firebaseUser)) {
        await loginOrRegisterUserWithDatabase();
      }
    }

    updateAcAdamyUser();
  }, [firebaseUser, acAdamyUser]);

  return {
    acAdamyUser,
    loginOrRegisterWithProvider,
    testeroni,
    logout,
    // etc.
  };
}
