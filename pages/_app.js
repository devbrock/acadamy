import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import '../css/tailwind.css';
import { AuthProvider } from '../hooks/auth.js';
import '../misc/fontAwesomeLibrary';

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <div className="text-gray-800">
        <Head>
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap"
            rel="stylesheet"
            key="google-fonts"
          />
        </Head>
        <AuthProvider>
          <Component {...pageProps} />
        </AuthProvider>
      </div>
    );
  }
}

export default MyApp;
