import React from 'react';
import Link from 'next/link';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Courses() {
  return (
    <Layout>
      <div className="px-24 py-24 ">
        <div className="flex justify-between">
          <div className="w-1/2 pr-4">
            <div className="flex">
              <p className=" flex align-middle items-center text-white bg-green-500 px-2 py-1 rounded">
                <FontAwesomeIcon icon={['fa', 'star']} className="h-4 mr-2" />{' '}
                Latest Course
              </p>
            </div>
            <h1 className="text-6xl font-bold">Minecraft Mods</h1>
            <p className="text-lg my-4">
              Ever wanted to make your own mod for a game? Always wanted to
              learn how to create a mod but never found an easy way to start?
            </p>
            <p className="text-lg my-4">
              Learn how Adam coded his own mod for Minecraft and integrated it
              with Twitch chat to create a new game mode called &quot;Animal
              Royale&quot;.
            </p>
            <Link href="/checkout" as={`/checkout`}>
              <a className="bg-indigo-700 text-white font-bold px-4 py-3 rounded shadow-lg">
                Buy Course
              </a>
            </Link>
            <Link href="/" as={`/`}>
              <a className="text-indigo-700 font-bold ml-4">
                View Course Details
              </a>
            </Link>
          </div>
          <div className="w-1/2 pl-4 bg-gray-300 rounded-lg"></div>
        </div>
      </div>
      <div className="px-24 py-24 ">
        <div className="mt-32">
          <h2 className="text-5xl font-bold mb-8">All courses</h2>
          <div className="flex">
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded h-full flex align-middle items-center justify-center">
                <div className="text-center px-8">
                  <p className="font-bold text-xl">More courses coming soon!</p>
                  <p className="text-sm my-4">
                    Check out the AdamLearns Twitch channel for the very latest
                    projects and VODs.
                  </p>
                  <div className="w-1/2 mx-auto">
                    <a
                      href="https://www.twitch.tv/Adam13531"
                      className=" flex align-middle justify-center items-center px-2 py-1 border-2 border-gray-400 rounded py-2"
                    >
                      <span className="font-bold">Go to twitch</span>
                      <FontAwesomeIcon
                        icon={['fa', 'external-link-alt']}
                        className="h-4 ml-2"
                      />{' '}
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {/* end card */}
          </div>
        </div>
      </div>
    </Layout>
  );
}
