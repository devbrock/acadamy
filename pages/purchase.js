import React, { useEffect } from 'react';
import Layout from '../components/Layout';
import firebase from 'firebase/app';
import 'firebase/functions';
import { CourseId } from '../misc/constants';

/**
 * This will ask a Cloud Function for Firebase for a pay link, then it will
 * start the checkout process using that link.
 * @param {CourseId} courseId
 */
async function startCheckoutProcess(courseId) {
  const makePayLinkFn = firebase.functions().httpsCallable('makePayLink');
  const result = await makePayLinkFn({ courseId });

  // We intentionally don't set disableLogout to true since the passthrough data
  // from the server will include the user's email address in The AcAdamy, so it
  // doesn't matter which email they use for the purchase itself (since that's
  // just where the receipt will go).
  window.Paddle.Checkout.open({
    override: result.data.payLinkUrl,
  });
}

/**
 * Note: at the time of writing, I don't use this for anything, but we may want
 * it in the future.
 * @see https://developer.paddle.com/reference/paddle-js/checkout-events
 */
const paddleEventHandler = ({ event, eventData }) => {};

export default function Purchase() {
  const paddleScriptId = '__paddle_script__';

  // Note: this effect will get called every time that we mount, but the Paddle
  // setup has an internal check to make sure it doesn't get called multiple
  // times, so it shouldn't be an issue.
  useEffect(() => {
    if (!document.getElementById(paddleScriptId)) {
      const script = document.createElement('script');
      script.id = paddleScriptId;
      script.src = 'https://cdn.paddle.com/paddle/paddle.js';
      script.defer = true;
      script.onload = () => {
        const vendorIdAsInt = parseInt(
          process.env.NEXT_STATIC_PADDLE_VENDOR_ID,
          10
        );
        window.Paddle.Setup({
          vendor: vendorIdAsInt,
          eventCallback: paddleEventHandler,
        });
      };
      document.head.append(script);
    }
  }, []);

  return (
    <div>
      <Layout>
        <button
          className="border bg-blue-500 text-white p-4"
          onClick={() => startCheckoutProcess(CourseId.PLACEHOLDER)}
        >
          Buy now!
        </button>
      </Layout>
    </div>
  );
}
