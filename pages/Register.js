import React from 'react';
import Link from 'next/link';
import OpenIDButton from '../components/OpenIDButton';
import { OpenIdProvidersForLoginOrRegistration } from '../misc/constants';
import _ from 'lodash';
import { useAuth } from '../hooks/auth.js';

export default function Register() {
  const auth = useAuth();

  return (
    <div className="p-10 bg-gray-100 min-h-screen">
      <div className="container mx-auto w-80 mb-6">
        <img src="/svg/acadamy-logo.svg" />
      </div>
      <div className="mx-auto bg-white max-w-xl pt-6 rounded-lg shadow-lg px-16">
        <div className="text-center font-bold text-xl mb-2">
          Sign up for your account
        </div>
        <div className="flex flex-wrap justify-center">
          {_.map(OpenIdProvidersForLoginOrRegistration, (provider) => (
            <OpenIDButton
              key={provider}
              openIdProvider={provider}
              onClick={auth.loginOrRegisterWithProvider}
            />
          ))}
        </div>
        <div className="border-t-2 mx-auto w-100 mt-6 py-4 text-center text-indigo-700 text-sm font-bold">
          <Link href="/login" as={`/login`}>
            <a>Already have an account? Log in</a>
          </Link>
        </div>
      </div>
      <div className="container flex justify-center mx-auto text-indigo-700 text-xs font-bold mt-10">
        <div className="mr-2">
          <a href="https://adamlearns.live/terms/privacy.html">
            Privacy Policy
          </a>
        </div>
        <div className="mr-2 text-gray-600">•</div>
        <div className="mr-2 last:mx-0">
          <a href="https://adamlearns.live/terms/terms.html">
            Terms of Service
          </a>
        </div>
      </div>
    </div>
  );
}
