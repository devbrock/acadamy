import React from 'react';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Checkout() {
  return (
    <Layout>
      <div className="my-24 flex justify-center">
        {/* Purchase Form Card */}
        <div className="shadow rounded-lg bg-white max-w-2xl shadow-lg mr-8">
          <div className="bg-indigo-700 text-white py-8 flex align-middle items-center pl-8 rounded-t-lg mb-8">
            <FontAwesomeIcon
              icon={['fa', 'credit-card']}
              className="h-10 mr-8"
            />
            <div>
              <h2 className="text-2xl">Complete your purchase</h2>
              <p className="text-sm text-indigo-200">
                select a payment method and enter your details to continue
              </p>
            </div>
          </div>
          <form className="px-24">
            <label className="font-bold text-lg">Select a payment method</label>
            <div className="flex flex-1 mt-2 mb-8">
              <div className="bg-gray-100 rounded font-bold py-2 px-3 mx-1 border-2 border-green-400 flex align-middle items-center cursor-pointer">
                <FontAwesomeIcon
                  icon={['fa', 'credit-card']}
                  className="h-4 mr-2"
                />
                Credit Card
              </div>
              <div className="bg-gray-100 rounded font-bold py-2 px-3 mx-1 border-2 border-gray-400 flex align-middle items-center cursor-pointer">
                <FontAwesomeIcon
                  icon={['fab', 'paypal']}
                  className="h-4 mr-2"
                />
                Paypal
              </div>
              <div className="bg-gray-100 rounded font-bold py-2 px-3 mx-1 border-2 border-gray-400 flex align-middle items-center cursor-pointer">
                <FontAwesomeIcon
                  icon={['fab', 'google-wallet']}
                  className="h-4 mr-2"
                />
                Google Pay
              </div>
            </div>

            <label className="font-bold text-lg">Name on Card</label>
            <input
              placeholder="John Smith"
              className="w-full border-2 border-gray-300 rounded-lg py-2 px-3 mt-2 mb-6 focus:outline-none"
            />

            <label className="font-bold text-lg">Card Number</label>
            <div className="mb-6 mt-2">
              <input
                placeholder="***"
                className="w-1/5 border-2 border-gray-300 rounded-lg mr-1 text-center py-2 px-4 focus:outline-none"
              />
              <input
                placeholder="***"
                className="w-1/5 border-2 border-gray-300 rounded-lg mx-1 text-center py-2 px-4 focus:outline-none"
              />
              <input
                placeholder="***"
                className="w-1/5 border-2 border-gray-300 rounded-lg mx-1 text-center py-2 px-4 focus:outline-none"
              />
              <input
                placeholder="***"
                className="w-1/5 border-2 border-gray-300 rounded-lg ml-1 text-center py-2 px-4 focus:outline-none"
              />
            </div>

            <div className="flex justify-start">
              <div>
                <label className="font-bold text-lg flex flex-col">
                  Expiration Date
                </label>
                <input
                  placeholder="MM/YY"
                  className="w-1/2 border-2 border-gray-300 rounded-lg text-center py-2 px-4 focus:outline-none"
                />
              </div>

              <div>
                <label className="font-bold text-lg flex flex-col">CVV</label>
                <input
                  placeholder="***"
                  className="w-1/2 border-2 border-gray-300 rounded-lg text-center py-2 px-4 focus:outline-none"
                />
              </div>
            </div>

            <div className="my-6">
              <input type="checkbox" className="mr-2" />
              <label>
                Redeem your <strong>$5</strong> of course credits with this
                purchase?
              </label>
            </div>

            {/* ! I wasnt sure how to do the gradient */}
            <button
              type="submit"
              onClick={() => alert('Get learning!')}
              className="bg-indigo-700 text-white py-3 px-4 rounded shadow-lg mb-8 flex align-middle items-center"
            >
              <FontAwesomeIcon
                icon={['fa', 'arrow-right']}
                className="h-4 mr-2"
              />
              Pay Now
            </button>
          </form>
        </div>

        {/* Order Summary Card */}
        <div className="shadow rounded-lg bg-white max-w-md shadow-lg flex-1">
          <div className="bg-indigo-700 text-white py-8 px-8 rounded-t-lg mb-8 ">
            <h2 className="text-2xl">Order Summary</h2>
            <p className="text-sm text-indigo-200">
              Please review your order in full.
            </p>
          </div>
          <div className="px-8">
            {/* order item 2 */}
            <div className="flex flex-1 justify-between mb-4">
              <div className="">
                <p>
                  Course: <span className="italic">Course Name</span>
                </p>
                <ul className="text-sm list-inside list-disc">
                  <li>x0 videos</li>
                  <li>x0 code repositories</li>
                  <li>x0 set of course notes</li>
                </ul>
              </div>
              <p className="font-bold">$00.00</p>
            </div>
            {/* order item 2 */}
            <div className="flex flex-1 justify-between mb-4">
              <div className="">
                <p>
                  Course: <span className="italic">Course Name</span>
                </p>
                <ul className="text-sm list-inside list-disc">
                  <li>x0 videos</li>
                  <li>x0 code repositories</li>
                  <li>x0 set of course notes</li>
                </ul>
              </div>
              <p className="font-bold">$00.00</p>
            </div>
            <div className="flex justify-between border-t-2 py-4">
              <p>Sales Tax</p>
              <p className="font-bold">$0.00</p>
            </div>
            <div className="flex justify-between border-t-2 border-b-2 py-4 text-xl">
              <p>Total</p>
              <p className="font-bold">$0.00</p>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
