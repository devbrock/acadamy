import React from 'react';
import Course from '../components/Course';
import Layout from '../components/Layout';
import Link from 'next/link';
import ReactPlayer from 'react-player';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * This is just a placeholder.
 */
export default function Index() {
  return (
    <Layout>
      <Course />
      <div className="p-24">
        <div className="flex justify-between">
          <div className="w-1/2 pr-4">
            <h1 className="text-6xl font-bold">
              A modern, interactive learning experience.
            </h1>
            <p className="text-lg my-4">
              AcAdamy is a premium library of learning material that is created
              in front of a live audience on the AdamLearns Twitch channel.
            </p>
            <ul className="text-lg my-4 list-inside list-disc">
              <li className="my-2">
                Watch courses live on Twitch and influence the decision-making
                process
              </li>
              <li className="my-2">
                Access videos, course notes and code repositories - all the
                material to get started quickly
              </li>
              <li className="my-2">
                Earn free course credits by becoming a Twitch subscriber or
                Patreon supporter
              </li>
            </ul>
            <div className="flex">
              <Link href="/courses" as={`/courses`}>
                <a className="bg-indigo-700 text-white font-bold px-4 py-3 rounded shadow-lg">
                  Browse Courses
                </a>
              </Link>
              <Link href="/" as={`/`}>
                <a className="text-indigo-700 font-bold ml-4 flex align-middle items-center">
                  Join the livestream
                  <FontAwesomeIcon
                    icon={['fa', 'external-link-alt']}
                    className="h-4 ml-2"
                  />
                </a>
              </Link>
            </div>
          </div>
          <div className="w-1/2 flex justify-center">
            <ReactPlayer url="https://www.twitch.tv/adam13531" playing />
          </div>
        </div>
      </div>
      <div className="px-24 pb-24 ">
        <div className="mt-32">
          <h2 className="text-5xl font-bold mb-8 text-center">
            Most Recent Courses
          </h2>
          <div className="flex justify-center">
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
            {/* start card */}
            <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
              <div className="bg-gray-300 rounded-t h-48"></div>
              <div className="py-4 px-8">
                <p className="text-sm text-indigo-700 font-bold">Type</p>
                <h6 className="text-xl font-bold">Course Title</h6>
                <label className="text-gray-600">Contents:</label>
                <ul className="list-inside list-disc text-gray-600">
                  <li>0 videos</li>
                  <li>0 Full Course Notes</li>
                  <li>0 Code Repository</li>
                </ul>
              </div>
            </div>
            {/* end card */}
          </div>
          <div className="mt-8 text-center">
            <Link href="/courses" as={`/courses`}>
              <a className="bg-indigo-700 text-white font-bold px-4 py-3 rounded shadow-lg">
                View All Courses
              </a>
            </Link>
          </div>
          <div className="w-3/4 flex align-middle items-center justify-center mx-auto text-center h-64 bg-gray-300 rounded mt-16 p-64">
            <h2 className="text-6xl font-bold">About Adam</h2>
          </div>
        </div>
      </div>
    </Layout>
  );
}
