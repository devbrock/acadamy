import React from 'react';
import Link from 'next/link';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ReactPlayer from 'react-player';

export default function Courses() {
  return (
    <Layout>
      <div className="bg-gray-100">
        <div className="px-24 pt-24 ">
          <div className="flex space-between">
            <h1 className="text-4xl flex-1 font-bold">Course Title</h1>
            <div>
              <a className="bg-indigo-700 text-white font-bold px-4 py-3 rounded shadow-lg">
                Buy Course
              </a>
            </div>
          </div>
          {/* start first section */}
          <div className="flex justify-between">
            <div className="w-2/3 pr-4 flex">
              <ReactPlayer
                url="https://www.youtube.com/watch?v=WDMchJQyh8o&t=31s"
                controls
                pip
                className="flex-1"
                height="100%"
              />
            </div>
            {/* side bar with other episodes in the course */}
            <div className="w-1/3 pl-4">
              {/* probably will want to break this out into like a thumbnail component or something similiar so this can be reused */}
              <div className="flex bg-white shadow-lg rounded-lg mb-4">
                <div className="bg-gray-300 w-1/4" />
                <div className="flex-col p-4 w-3/4">
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    Episode Number
                  </p>
                  <p className="font-bold text-lg">Episode Title</p>
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    00 Minutes 00 Seconds
                  </p>
                </div>
              </div>
              <div className="flex bg-white shadow-lg rounded-lg mb-4">
                <div className="bg-gray-300 w-1/4" />
                <div className="flex-col p-4 w-3/4">
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    Episode Number
                  </p>
                  <p className="font-bold text-lg">
                    This is a longer Episode Title, its like really long
                  </p>
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    00 Minutes 00 Seconds
                  </p>
                </div>
              </div>
              <div className="flex bg-white shadow-lg rounded-lg mb-4">
                <div className="bg-gray-300 w-1/4" />
                <div className="flex-col p-4 w-3/4">
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    Episode Number
                  </p>
                  <p className="font-bold text-lg">Episode Title</p>
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    00 Minutes 00 Seconds
                  </p>
                </div>
              </div>
              <div className="flex bg-white shadow-lg rounded-lg mb-4">
                <div className="bg-gray-300 w-1/4" />
                <div className="flex-col p-4 w-3/4">
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    Episode Number
                  </p>
                  <p className="font-bold text-lg">
                    This is a longer Episode Title, its like really long
                  </p>
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    00 Minutes 00 Seconds
                  </p>
                </div>
              </div>
              <div className="flex bg-white shadow-lg rounded-lg mb-4">
                <div className="bg-gray-300 w-1/4" />
                <div className="flex-col p-4 w-3/4">
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    Episode Number
                  </p>
                  <p className="font-bold text-lg">Episode Title</p>
                  <p className="font-bold text-gray-600 uppercase text-sm">
                    00 Minutes 00 Seconds
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="w-2/3 flex pr-4 mt-4">
            <div className="w-2/3">
              <p className="text-gray-900 text-lg">
                Ut quis laborum reprehenderit proident sint. Consequat ipsum ea
                laborum nulla incididunt. Laborum adipisicing anim laborum qui
                labore. Exercitation ad reprehenderit veniam ad eu veniam duis
                sit.Sunt occaecat consequat voluptate id occaecat qui sint
                exercitation sint eiusmod aliqua ex. Tempor velit dolore sunt
                excepteur eu ut sint nostrud minim enim id in tempor. Tempor
                voluptate labore incididunt cillum reprehenderit irure elit esse
                aliquip ea exercitation fugiat.
              </p>
              <div className="mt-6">
                <a className="bg-indigo-700 text-white font-bold px-4 py-3 rounded shadow-lg">
                  Buy Course
                </a>
              </div>
            </div>
            <ul className="w-1/3 border-l-2 pl-8">
              <li className="my-4">
                <a href="#" className="text-indigo-700 font-bold">
                  Course Prerequisites
                </a>
              </li>
              <li className="my-4">
                <a href="#" className="text-indigo-700 font-bold">
                  Return Policy
                </a>
              </li>
              <li className="my-4">
                <a href="#" className="text-indigo-700 font-bold">
                  Licencing
                </a>
              </li>
            </ul>
          </div>
          {/* end first section */}
        </div>
        <div className="px-24 pb-24">
          <div className="mt-32">
            <h2 className="text-5xl font-bold mb-8">Related courses</h2>
            <div className="flex">
              {/* start card */}
              <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
                <div className="bg-gray-300 rounded-t h-48"></div>
                <div className="py-4 px-8">
                  <p className="text-sm text-indigo-700 font-bold">Type</p>
                  <h6 className="text-xl font-bold">Course Title</h6>
                  <label className="text-gray-600">Contents:</label>
                  <ul className="list-inside list-disc text-gray-600">
                    <li>0 videos</li>
                    <li>0 Full Course Notes</li>
                    <li>0 Code Repository</li>
                  </ul>
                </div>
              </div>
              {/* end card */}
              {/* start card */}
              <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
                <div className="bg-gray-300 rounded-t h-48"></div>
                <div className="py-4 px-8">
                  <p className="text-sm text-indigo-700 font-bold">Type</p>
                  <h6 className="text-xl font-bold">Course Title</h6>
                  <label className="text-gray-600">Contents:</label>
                  <ul className="list-inside list-disc text-gray-600">
                    <li>0 videos</li>
                    <li>0 Full Course Notes</li>
                    <li>0 Code Repository</li>
                  </ul>
                </div>
              </div>
              {/* end card */}
              {/* start card */}
              <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
                <div className="bg-gray-300 rounded-t h-48"></div>
                <div className="py-4 px-8">
                  <p className="text-sm text-indigo-700 font-bold">Type</p>
                  <h6 className="text-xl font-bold">Course Title</h6>
                  <label className="text-gray-600">Contents:</label>
                  <ul className="list-inside list-disc text-gray-600">
                    <li>0 videos</li>
                    <li>0 Full Course Notes</li>
                    <li>0 Code Repository</li>
                  </ul>
                </div>
              </div>
              {/* end card */}
              {/* start card */}
              <div className="bg-white shadow-lg flex-1 rounded-lg max-w-md mx-4">
                <div className="bg-gray-300 rounded h-full flex align-middle items-center justify-center">
                  <div className="text-center px-8">
                    <p className="font-bold text-xl">
                      More courses coming soon!
                    </p>
                    <p className="text-sm my-4">
                      Check out the AdamLearns Twitch channel for the very
                      latest projects and VODs.
                    </p>
                    <div className="w-1/2 mx-auto">
                      <a
                        href="https://www.twitch.tv/Adam13531"
                        className=" flex align-middle justify-center items-center px-2 py-1 border-2 border-gray-400 rounded py-2"
                      >
                        <span className="font-bold">Go to twitch</span>
                        <FontAwesomeIcon
                          icon={['fa', 'external-link-alt']}
                          className="h-4 ml-2"
                        />{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              {/* end card */}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
